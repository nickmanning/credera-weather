import daysofweek from '../../constants/daysofweek.js';

function ShowDate(props){
    const day = daysofweek[props.date.getDay() % 7];
    const month = ['Jan','Feb','March','April','May','June','July','Aug','Sept','Oct','Nov','Dec'][props.date.getMonth() % 12];
    const date = props.date.getDate();
    const year = props.date.getFullYear();
    return <>{day}, {month} {date}, {year}</>
  }

  export default ShowDate;