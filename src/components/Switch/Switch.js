import './Switch.css';
import {useState} from 'react';

function Switch(props){
    const [onState,setOnState] = useState(props.initialState);
    return <switch-custom class={onState} onClick={()=>{
        setOnState(onState==='on'?'off':'on');
        if (onState==='off') props.onSwitchOn()
        else{
            props.onSwitchOff()
        }
    }}>
        <choice-one>{props.choiceOne}</choice-one>
        <choice-two>{props.choiceTwo}</choice-two>
    </switch-custom>;
}

export default Switch;