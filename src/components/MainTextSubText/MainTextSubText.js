import './MainTextSubText.css';

function MainTextSubText(props) {
  return (
    <main-text-sub-text>
      <div className='main-text'>{props.main}</div>   
      <div className='sub-text'>{props.sub}</div>   
    </main-text-sub-text>
  );
}

export default MainTextSubText;
