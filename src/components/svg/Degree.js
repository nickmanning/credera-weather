function Degree(){
    return <degree><svg width="12px" height="12px" viewBox="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Weather" transform="translate(-261.000000, -215.000000)" stroke="#65AED5" stroke-width="2.5">
            <g id="TODAY" transform="translate(205.000000, 195.000000)">
                <circle id="Oval-2-Copy" cx="62" cy="26" r="4"></circle>
            </g>
        </g>
    </g>
</svg></degree>
}

export default Degree;