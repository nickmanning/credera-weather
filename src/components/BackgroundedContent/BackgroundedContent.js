import './BackgroundedContent.css';

function BackgroundedContent(props){
    return <backgrounded-content>
        {props.children}
    </backgrounded-content>
}

export default BackgroundedContent;