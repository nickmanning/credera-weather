//import logo from './logo.svg';
import './SiteFrame.css';

function SiteFrame(props) {
  return (
    <site-frame>
        <header>
            {props.headerContent}
        </header>
        <app-main>
            {props.bodyContent}
        </app-main>
    </site-frame>
  );
}

export default SiteFrame;
