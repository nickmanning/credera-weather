import './App.css';

import SiteFrame from './components/SiteFrame/SiteFrame.js';
import MainTextSubText from './components/MainTextSubText/MainTextSubText.js';
import LeftRightContent from './components/LeftRightContent/LeftRightContent.js';
import BackgroundedContent from './components/BackgroundedContent/BackgroundedContent.js';
import Switch from './components/Switch/Switch.js';
import Card from './components/Card/Card.js';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

import axios from 'axios';
import {useState, useEffect} from 'react';

import ShowDate from './components/ShowDate/ShowDate.js';
import daysofweek from './constants/daysofweek.js';

import Flex from './components/Flex/Flex.js';
import Cloud from './components/svg/Cloud.js';
import Degree from './components/svg/Degree.js';

function abbrivateRegion(region){
  return ({
    'Texas':'TX'
  })[region] || region
}

function App() {

  const [switchState, setSwitchState] = useState('on');
  const [city,setCity] = useState('Dallas');//city -> response -> all other data
  const LOADING = 'Loading...';
  const [response,setResponse] = useState({
    data:{
      location:{
        region:LOADING,
      },
      current:{
        temp_f:LOADING,
        temp_c:LOADING,
        condition:{
          text:LOADING,
        },
        wind_mph:LOADING
      },
      forecast:{
        forecastday:[
          {
            day:{
              condition:{
                text:LOADING
              }
            }
          },
          {
            day:{
              condition:{
                text:LOADING
              }
            }
          },
          {
            day:{
              condition:{
                text:LOADING
              }
            }
          },
          {
            day:{
              condition:{
                text:LOADING
              }
            }
          },
          {
            day:{
              condition:{
                text:LOADING
              }
            }
          }
        ]
      }
    }
  });
  
  useEffect(() => {
    axios.get(`https://api.weatherapi.com/v1/forecast.json?key=6fad7d51c75f4aca8b832356212005&q=${city}&days=6&aqi=no&alerts=no`).then(function (response) {
      console.log(response)
      setResponse(response);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    });
  },[city]);

  

  return (
    <SiteFrame

      headerContent={
        <MainTextSubText 
          main={<><FontAwesomeIcon icon={faMapMarkerAlt} onClick={()=>{
            setCity(prompt('Enter a new city'));
          }}/> {city}, {abbrivateRegion(response.data.location.region)}</>}
          sub={<ShowDate date={new Date()} />}
        />
      }

      bodyContent={
        <>
          <BackgroundedContent>
            <Cloud type="1" />
            <Cloud type="2" />
            <LeftRightContent 
              left={<div style={{display:'flex',alignItems:'center'}}>
                
                <temperature>
                  {Math.round(switchState === 'off' ? response.data.current.temp_c : response.data.current.temp_f)} <Degree />
                </temperature>
                <condition>
                  <img src={response.data.current.condition.icon} alt={response.data.current.condition.text}/>
                </condition>
                <right>
                  <condition>
                    {response.data.current.condition.text}
                  </condition>
                  <wind>{Math.round(response.data.current.wind_mph)} mph</wind>
                </right>
              </div>}
              right={<Switch
                initialState='on'
                choiceOne='C'
                choiceTwo='F'
                onSwitchOn={()=>{
                  setSwitchState('on')
                }}
                onSwitchOff={()=>{
                  setSwitchState('off')              
                }}
              />}
            />
          </BackgroundedContent>
          <Flex>
            {response.data.forecast.forecastday.slice(1).map((forecastday,i)=>{
              const day = daysofweek[((new Date()).getDay()+i+1) % 7].substr(0,3);
              return <Card key={day}>
                <div>
                  {day}
                </div>
                <div>
                  <img src={forecastday.day.condition.icon} alt={forecastday.day.condition.text} />
                </div>
                <div>
                  {Math.round(switchState === 'off' ? forecastday.day.maxtemp_c : forecastday.day.maxtemp_f)}º
                </div>
              </Card>
            })}
          </Flex>
        </>
      }

    />   
  );
}



export default App;
